use serde::{Deserialize, Serialize};
use serde_json::Result;
use std::collections::HashMap;
use std::process;
use std::path::Path;
use std::fs::File;
use std::io::prelude::*;
use structopt::StructOpt;

#[derive(Serialize, Deserialize, Clone)]
pub struct Settings {
	index_settings: IndexSettings,
	show_results: ShowResults,
	auto_open: AutoOpenResults,
}

impl Settings {
	pub fn from_json(json: &str) -> core::result::Result<Settings, &'static str> {
		let s: Settings = match serde_json::from_str(json) {
			Ok(p) => p,
			Err(err) => {
				println!("Err: {}", err);
				return Err("Error while parsing json!");
			},
		};
		
		Ok(s)
	}
	
	pub fn default() -> Settings {
		Settings {
			index_settings: IndexSettings::new(),
			show_results: ShowResults::Top(10),
			auto_open: AutoOpenResults::None,
		}
	}
	
	pub fn to_json(&self) -> core::result::Result<String, &'static str> {
		match serde_json::to_string(&self) {
			Ok(s) => Ok(s),
			Err(err) => {
				println!("Err: {}", err);
				return Err("Error while parsing json!");
			},
		}
	}
	
	pub fn index_location(&self) -> String {
		self.index_settings.index_location.clone()
	}

    pub fn set_index_location(&self, location: String) -> Settings {
        let mut new_settings = self.clone();
        
        new_settings.index_settings.index_location = location;

        new_settings
    }
	
	pub fn add_indexing_path(&mut self, name: &str, path: &str) -> core::result::Result<(), &'static str> {
        if self.index_settings.indexing_paths.contains_key(&String::from(name)) {
            return Err("Path name dupplicated!")
        }

        self.index_settings.indexing_paths.insert(String::from(name), String::from(path));

        Ok(())
    } 

    pub fn remove_indexing_path(&mut self, name: &str) -> Option<String> {
        self.index_settings.indexing_paths.remove(&String::from(name))
    }

    pub fn indexing_paths(&self) -> &HashMap<String, String> {
        &self.index_settings.indexing_paths
    }

    pub fn set_show_results(&self, show_results: ShowResults) -> Settings {
        let mut new_settings = self.clone();
        
        new_settings.show_results = show_results;

        new_settings
    }

    pub fn show_results(&self) -> &ShowResults {
        &self.show_results
    }

    pub fn set_auto_open(&self, auto_open: AutoOpenResults) -> Settings {
        let mut new_settings = self.clone();
        
        new_settings.auto_open = auto_open;

        new_settings
    }

    pub fn auto_open(&self) -> &AutoOpenResults {
        &self.auto_open
    }
}

#[derive(Serialize, Deserialize, Clone, StructOpt)]
pub struct IndexSettings {
	index_location: String,
	indexing_paths: HashMap<String, String>,
}

impl IndexSettings {
	pub fn new() -> IndexSettings {
		IndexSettings {
			index_location: String::new(),
			indexing_paths: HashMap::new(),
		}
	}
}

#[derive(Serialize, Deserialize, Copy, Clone, StructOpt)]
pub enum ShowResults {
	None,
	Top(u32),
	All,
}

impl ShowResults {
	pub fn is_none(&self) -> bool {
		match self {
			ShowResults::None => true,
			_ => false,
		}
	}

	pub fn is_top(&self) -> bool {
		match self {
			ShowResults::Top(_) => true,
			_ => false,
		}
	}

	pub fn is_num(&self, num: u32) -> bool {
		match self {
			ShowResults::Top(num) => true,
			_ => false,
		}
	}

	pub fn is_all(&self) -> bool {
		match self {
			ShowResults::All => true,
			_ => false,
		}
	}
}

#[derive(Serialize, Deserialize, Copy, Clone, StructOpt)]
pub enum AutoOpenResults {
	None,
	First,
	At(u32),
	Last,
}

impl AutoOpenResults {
	pub fn is_none(&self) -> bool {
		match self {
			AutoOpenResults::None => true,
			_ => false,
		}
	}

	pub fn is_first(&self) -> bool {
		match self {
			AutoOpenResults::First => true,
			_ => false,
		}
	}

	pub fn is_at(&self) -> bool {
		match self {
			AutoOpenResults::At(_) => true,
			_ => false,
		}
	}

	pub fn is_num(&self, num: u32) -> bool {
		match self {
			AutoOpenResults::At(num) => true,
			_ => false
		}
	}

	pub fn is_last(&self) -> bool {
		match self {
			AutoOpenResults::Last => true,
			_ => false,
		}
	}
}

pub fn write_settings(settings: Settings, file_location: &str) -> core::result::Result<(), &'static str> {
    let mut file = match File::create(file_location) {
		Ok(f) => f,
		Err(err) => {
			println!("Error received while creating file: {}", err);
			return Err("Could not create file!");
		},
	};

	let json = match settings.to_json() {
		Ok(j) => j,
		Err(err) => {
			println!("Error received while parsing settings: {}", err);
			return Err("Could not parse settings!");
		},
	};

	if let Err(err) = file.write_all(json.as_str().as_bytes()){
		println!("Error received while writing file: {}", err);
		return Err("Could not write file!");
	}

	Ok(())
}

pub fn read_settings(file_location: &str) -> core::result::Result<Settings, &'static str> {
	let json = match std::fs::read_to_string(file_location) {
		Ok(j) => j,
		Err(err) => {
			println!("Error received while reading settings file: {}", err);
			return Err("Could not read settings file!");
		},
	};

    Settings::from_json(&json)
}

pub fn run(file_location: &str, cmd_args: CmdArgs) {
	if !Path::new(file_location).exists() {
		let settings = Settings::default();

		match write_settings(settings, file_location) {
			Ok(_) => println!("No settings file found! A new one has been created... "),
			Err(err) => {
				println!("No settings file found! A try to create a new one failed: ");
				println!("{}", err);
				return;
			},
		}
	}

	let mut settings = read_settings(file_location).unwrap();

	if can_run_args(&args) {
		let new_settings = run_args(&args, settings);

		if args.continue_menu {
			run_menu(mut settings);
		}

		write_settings(new_settings, file_location);
	}
	else {
		let new_settings = run_menu(settings);

		write_settings(new_settings, file_location);
	}
}

pub fn can_run_args(args: &CmdArgs) -> bool {
	args.index_location().is_some() || 
	args.indexing_paths().is_some() || 
	args.show_results().is_some() || 
	args.auto_open().is_some() 
} 

pub fn run_args(args: &CmdArgs, settings: Settings) -> Settings {
	let mut new_settings = settings.clone();
	
	if let Some(x) = args.indexing_paths() {
		match x {
			IndexingPaths::Add {name, value} => new_settings.add_indexing_path(name, value),
			IndexingPaths::Remove {name} => new_settings.remove_indexing_path(name),
			IndexingPaths::Display => {
				println!("The following paths were found. \n");

				for (name, path) in new_settings.indexing_paths() {
					println!("Name: {}, Path: {}", name, path);
				}
			},
		}
		println!("Updated indexing paths...");
	}

	if let Some(x) = args.index_location() {
		new_settings = new_settings.set_index_location(x);
		println!("Updated index file location...");
	}

	if let Some(x) = args.show_results() {
		new_settings = new_settings.set_show_results(x);
		println!("Updated result showing settings...");
	}

	if let Some(x) = args.auto_open() {
		new_settings = new_settings.set_auto_open(x);
		println!("Updated automatic opening settings...");
	}

	new_settings
}

pub fn run_menu(settings: Settings) -> Settings {
	display_help_text();
	let mut new_settings = settings.clone();

	loop {
		let mut user_input = "";

		let input_parts = user_input.split(' ').collect();

		if input_parts.len() > 0 {
			match input_parts[0].to_lowercase().as_str() {
				"exit" => return new_settings,
				"location" => new_settings = change_location(),
				"show_res" => new_settings = change_show_res(),
				"auto_open" => new_settings = change_auto_open(),
				"path" => new_settings = change_paths(),
				"help" => display_help_text(),
				_ => println!("Unsupported command entered!");
			};
		}
	}
}

pub fn change_location(settings: Settings, parts: Vec<&str>) -> Settings {
	if parts.len == 1 {
		settings.set_index_location(parts[1]);
	}
}

pub fn change_show_res(settings: Settings, parts: Vec<&str>) -> Settings {

}

pub fn change_paths(settings: Settings, parts: Vec<&str>) -> Settings {

}

pub fn change_auto_open(settings: Settings, parts: Vec<&str>) -> Settings {

}

pub fn display_help_text() {
	println!("To change settings please enter 1 of the following commands! ");
	println!("");
	println!("Display all options: help");
	println!("Change index file location: location [<\"new_file_locatuin\">| help]");
	println!("Change display of results: show_res [none | top <\"num_of_files\"> | all | help]");
	println!("Change auto opening of files: auto_open [none | first | at <\"file_position\"> | last | help]");
	println!("Change indexing paths: path [add <\"name\"> <\"value\"> | remove <\"name\"> | display | help ]");
	println!("Close Program: exit ");
}

#[derive(Debug, StructOpt)]
#[structopt(name = "bs_config_editor", about = "Editor für settings files von better startup.")]
pub struct CmdArgs {
	#[structopt(subcommand)]
	indexing_paths: Option<IndexingPaths>,

	#[structopt(short = "l", long = "location")]
	index_location: Option<String>,

	#[structopt(subcommand)]
	show_results: Option<ShowResults>,

	#[structopt(subcommand)]
	auto_open: Option<AutoOpenResults>,

	#[structopt(short = "c", long = "continue")]
	continue_menu: bool,
}

impl CmdArgs {
	pub fn new() -> CmdArgs {
		CmdArgs::from_args()
	}

	pub fn indexing_paths(&self) -> &Option<IndexingPaths> {
		&self.IndexingPaths
	}

	pub fn index_location(&self) -> &Option<String> {
		&self.index_location
	}

	pub fn show_results(&self) -> &Option<ShowResults> {
		&self.show_results
	}

	pub fn auto_open(&self) -> &Option<AutoOpenResults> {
		&self.auto_open
	}

	pub fn continue_menu(&self) -> bool {
		self.continue_menu
	}
}

#[derive(StructOpt)]
pub enum IndexingPaths {
	Add {
		name: String,
		value: String,
	},
	Remove {
		name: String,
	},
	Display,
}

fn main() {
    println!("{} v.{}\n", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));
	let args = CmdArgs::new();

	run("./settings.json", args);

	println!("press any key to continue ...");
	pause();
}

fn pause() {
    let _ = process::Command::new("cmd.exe")
        .arg("/c")
        .arg("pause")
        .status();
}